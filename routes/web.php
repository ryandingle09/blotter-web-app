<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Auth::routes();

Route::middleware(['auth'])->group(function(){
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

    Route::prefix('/account')->group(function(){
        Route::get('/', 'AccountController@index')->name('account');
    });

    Route::prefix('/users')->group(function(){
        Route::get('/', 'UserController@index')->name('user-list');
    });

    Route::prefix('/hearing')->group(function(){
        Route::get('/', 'HearingController@index')->name('hearing');
        Route::get('{complaint_id}/create', 'HearingController@create')->name('create-hearing');
        Route::post('{complaint_id}/create', 'HearingController@store')->name('store-hearing');
        Route::get('{id}/edit', 'HearingController@edit')->name('edit-hearing');
        Route::put('{id}/edit', 'HearingController@update')->name('update-hearing');
        Route::get('{id}/show', 'HearingController@show')->name('show-hearing');
        Route::get('{id}/done', 'HearingController@done')->name('done-hearing');
    });

    Route::prefix('/complaint')->group(function(){
        Route::get('/', 'ComplaintController@index')->name('complaint');
        Route::get('/create', 'ComplaintController@create')->name('create-complaint');
        Route::post('/create', 'ComplaintController@store')->name('store-complaint');
        Route::get('{id}/edit', 'ComplaintController@edit')->name('edit-complaint');
        Route::put('{id}/edit', 'ComplaintController@update')->name('update-complaint');
        Route::get('{id}/show', 'ComplaintController@show')->name('show-complaint');
        Route::get('{id}/done', 'ComplaintController@done')->name('done-complaint');
        Route::delete('{id}/delete', 'ComplaintController@destroy')->name('delete-complaint');
    });
});
