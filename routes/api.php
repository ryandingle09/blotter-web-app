<?php

//header("Access-Control-Allow-Origin", "*");//apache
header("Access-Control-Allow-Origin: *");//nginx
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Methods: POST, GET, OPTIONS, DELETE, PUT, PATCH");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, If-Modified-Since, Cache-Control, Pragma, x-xsrf-token, x-csrf-token, token");
header("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");

//use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return dd($request->user());
});
*/

Route::prefix('v1')->group(function(){

    //token $request->get('api_token');

    /* LOGIN API ROUTES */
    Route::post('login', 'API\AuthController@login');

    /* REGISTER API ROUTES */
    Route::post('register', 'API\AuthController@register');

    /* COMPLAINT ROUTES */
    Route::get('complaint', 'API\ComplaintController@index');
    Route::post('/complaint/store', 'API\ComplaintController@store');
    Route::post('/{id}/complaint/update', 'API\ComplaintController@update');
    Route::get('/{id}/complaint/edit', 'API\ComplaintController@edit');
    Route::get('/{id}/complaint/show', 'API\ComplaintController@show');
    Route::post('/{id}/complaint/delete', 'API\ComplaintController@destroy');

    /* HEARING API ROUTES */
    Route::get('hearing', 'API\HearingController@index');

    /* ACCOUNT API ROUTES */
    Route::get('account/show', 'API\AccountController@show');
});
