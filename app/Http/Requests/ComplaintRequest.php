<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class ComplaintRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $rules = [
            'complainant'           => 'required|max:255',
            'complainant_age'       => 'required|max:2',
            'complainant_address'   => 'required',
            'respondent'            => 'required|max:255',
            'respondent_age'        => 'required|max:2',
            'respondent_address'    => 'required',
            'statement'             => 'required',
        ];
        
        #if ($request->hasFile('evidence')) 
        #    $rules['evidence'] = 'mimes:jpeg,bmp,png,jpg,gif,mp4,mpeg,avi,quicktime';

        return $rules;
    }
}
