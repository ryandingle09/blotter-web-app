<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\User;

class AuthController extends Controller
{
    //login method via api
    public function login(Request $request)
    {
        $this->validate($request, [
            'username'  => 'required|max:255',
            'password'  => 'required|max:255',
        ]);

        if (\Auth::attempt(['username' => $request->username, 'password' => $request->password])) 
        {
            $data = User::where('username', $request->username)->get()[0];

            $user = [
                'id'            => $data->id,
                'firstname'     => $data->firstname,
                'middlename'    => $data->username,
                'lastname'      => $data->lastname,
                'username'      => $data->username,
                'email'         => $data->email,
                'address'       => $data->address,
                'birthdate'     => $data->birthdate,
                'api_token'     => $data->api_token,
            ];

            return response()->json($user, 200);
        }
        else
        {
            return response()->json(['status', 'Invalid credentials.'], 422);
        }
    }

    //register method via api
    public function register(Request $request)
    {
        $this->validate($request, [
            'username'      => 'required|max:255|unique:users',
            'password'      => 'required|string|min:6|confirmed',
        ]);

        User::create([
            'username'  => $request->username,
            'password'  => Hash::make($request->password),
            'api_token' => Hash::make($request->username.''.date('Y-m-d m:i:s')),
        ]);
        
        $data = User::where('username', $request->username)->get()[0];

        $user = [
            'id'            => $data->id,
            'firstname'     => $data->firstname,
            'middlename'    => $data->username,
            'lastname'      => $data->lastname,
            'username'      => $data->username,
            'email'         => $data->email,
            'address'       => $data->address,
            'birthdate'     => $data->birthdate,
            'api_token'     => $data->api_token,
        ];

        return response()->json($user, 200);
    }

    //api token checker
    public static function checkToken($id, $token) 
    {
        $data = User::find($id);

        if(!$data) return response()->json(['status' => 'invalid-token'], 422); 
        if($data->api_token == $token)
        {
            return true;
        }
        else
        {
            return response()->json(['status' => 'invalid-token'], 422);
        }
    }
}
