<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Hearing;
use App\Complaint;
use App\Http\Controllers\API\AuthController;

class HearingController extends Controller
{
    public $user_id;

    function __construct(Request $request){
        //AuthController::checkToken($request->get('user'), $request->get('token'));
        $this->user_id = $request->get('user');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $offset = $request->get('offset');

        // $data = DB::select('
        //     select h.*  where active = ?'
        // , [1]);
        
        $data = DB::table('hearings')
                ->join('complaints', 'hearings.complaint_id', '=', 'complaints.id')
                ->select('hearings.*')
                ->where('complaints.user_id', $this->user_id)
                ->orderBy('id', 'DESC')
                ->orderBy('date', 'ASC')
                ->limit(10)->offset($offset)->get();
        
        if($request->get('search'))
        {
            $data = DB::table('hearings')
                ->join('complaints', 'hearings.complaint_id', '=', 'complaints.id')
                ->select('hearings.*')
                ->where('complaints.user_id', $this->user_id)
                ->orWhere('hearings.status', 'like', '%'.$request->get('search').'%')
                ->orWhere('hearings.date', 'like', '%'.$request->get('search').'%')
                ->orWhere('hearings.time', 'like', '%'.$request->get('search').'%')
                ->orderBy('hearings.id', 'DESC')
                ->orderBy('hearings.date', 'ASC')
                ->limit(10)->offset($offset)->get();
        }

        return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
