<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ComplaintRequest;
use App\Complaint;
use App\Http\Controllers\API\AuthController;

class ComplaintController extends Controller
{
    public $user_id;

    function __construct(Request $request){
        //AuthController::checkToken($request->get('user'), $request->get('token'));
        $this->user_id = $request->get('user');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $offset = $request->get('offset');

        $data = Complaint::where('user_id',$this->user_id)->limit(10)->with('hearing')->offset($offset)->orderBy('id', 'DESC')->get();
        
        if($request->get('search')) 
        {
            $data = Complaint::where('user_id',$this->user_id)
                    ->orWhere('complainant', 'like', '%'.$request->get('search').'%')
                    ->orWhere('respondent', 'like', '%'.$request->get('search').'%')
                    ->orWhere('complainant_age', 'like', '%'.$request->get('search').'%')
                    ->orWhere('respondent_age', 'like', '%'.$request->get('search').'%')
                    ->orWhere('complainant_address', 'like', '%'.$request->get('search').'%')
                    ->orWhere('respondent_address', 'like', '%'.$request->get('search').'%')
                    ->orWhere('hearing_stage', 'like', '%'.$request->get('search').'%')
                    ->orWhere('status', 'like', '%'.$request->get('search').'%')
                    ->with('hearing')
                    ->orderBy('id', 'DESC')
                    ->limit(10)->offset($offset)->get();
        }

        //return dd($data);
        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ComplaintRequest $request)
    {
        $model = new Complaint;

        if ($request->hasFile('evidence')) 
        {
            $extensions = ['jpeg','bmp','png','jpg','gif','mp4','mpeg','avi','quicktime'];

            //check extensions
            foreach ($request->evidence as $photo)
            {
                $extension  = $photo->getClientOriginalExtension();
                $check      = in_array($extension, $extensions);

                if(!$check) return back()->withInput()->with('evidence', 'Upload file must be an jpeg|bmp|png|jpg|gif|mp4|mpeg|avi|quicktime file extensiont or valid image/video');

                $ext[]  = $check;
            }

            //set images
            foreach ($request->evidence as $photo)
            {
                $filename = $photo->store('files', 'public');
                $data[]     = url('/').'/storage/'.$filename;
            }
            
            $model->file  = json_encode($data);
        }

        $model->user_id             = $this->user_id;
        $model->complainant         = $request->complainant;
        $model->complainant_age     = $request->complainant_age;
        $model->complainant_address = $request->complainant_address;
        $model->respondent          = $request->respondent;
        $model->respondent_age      = $request->respondent_age;
        $model->respondent_address  = $request->respondent_address;
        $model->statement           = $request->statement;
        $model->added_by            = $this->user_id;
        $model->save();

        return response()->json(['status' => 'success'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Complaint::find($id);
        return response()->json([$data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Complaint::find($id);
        return response()->json([$data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ComplaintRequest $request, $id)
    {
        $model                      = Complaint::find($id);
        
        if ($request->hasFile('evidence')) 
        {
            $extensions = ['jpeg','bmp','png','jpg','gif','mp4','mpeg','avi','quicktime'];

            //check extensions
            foreach ($request->evidence as $photo)
            {
                $extension  = $photo->getClientOriginalExtension();
                $check      = in_array($extension, $extensions);

                if(!$check) return back()->withInput()->with('evidence', 'Upload file must be an jpeg|bmp|png|jpg|gif|mp4|mpeg|avi|quicktime file extensiont or valid image/video');

                $ext[]  = $check;
            }

            //remove old images if has uploaded
            if(!is_null($model->file))
            {
                foreach(json_decode($model->file) as $photo)
                {
                    $db_image = str_replace(url('/').'/storage', storage_path('app/public'), $photo);
                    #return $db_image;
                    if(file_exists($db_image)) unlink($db_image);
                }
            }

            //update new images
            foreach ($request->evidence as $photo)
            {
                $filename = $photo->store('files', 'public');
                $data[]     = url('/').'/storage/'.$filename;
            }
            
            $model->file  = json_encode($data);
        }

        $model->complainant         = $request->complainant;
        $model->complainant_age     = $request->complainant_age;
        $model->complainant_address = $request->complainant_address;
        $model->respondent          = $request->respondent;
        $model->respondent_age      = $request->respondent_age;
        $model->respondent_address  = $request->respondent_address;
        $model->statement           = $request->statement;
        $model->updated_by          = $this->user_id;
        $model->save();

        return response()->json(['status' => 'success'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Complaint::find($id);
        $model->delete();

        return response()->json(['status' => 'success'], 200);
    }
}
