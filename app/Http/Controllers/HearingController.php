<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\HearingRequest;
use App\Hearing;

class HearingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->get('search'))
        {
            $hearings = Hearing::where('complaint_id', 'like', '%'.$request->get('search').'%')
                ->orWhere('status', 'like', '%'.$request->get('search').'%')
                ->orWhere('date', 'like', '%'.$request->get('search').'%')
                ->orWhere('time', 'like', '%'.$request->get('search').'%')
                ->orderBy('id', 'DESC')
                ->orderBy('date', 'ASC')
                ->with('complaint')->paginate(10);
            
            $hearings->withPath('?search='.$request->get('search'));
        }
        else
        {
            $hearings = Hearing::orderBy('id', 'DESC')->with('complaint')->paginate(10);
        }

        return view('hearing.index', ['hearings' => $hearings]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $data = \App\Complaint::find($id);

        return view('hearing.create', [
            'id' => $id, 
            'data' => $data
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HearingRequest $request, $id)
    {
        $model                  = new Hearing;
        $model->complaint_id    = $id;
        $model->date            = date('Y-m-d', strtotime($request->date));
        $model->time            = $request->time;
        $model->added_by        = \Auth::id();
        $model->save();

        $model2                 = \App\Complaint::find($id);
        $model2->hearing_stage  = count(Hearing::where(['complaint_id' => $id, 'status' => 'done'])->get());
        $model2->save();

        return redirect('hearing')->with(['alert' => 'success', 'message' => 'Hearing successfully added.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Hearing::find($id);

        return view('hearing.show', [
            'data' => $data, 
            'id' => $id
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Hearing::find($id);
        $data2 = \App\Complaint::find($data->complaint_id);

        return view('hearing.edit', [
            'data' => $data, 
            'data2' => $data2, 
            'id' => $id
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(HearingRequest $request, $id)
    {
        $model                  = Hearing::find($id);
        $model->complaint_id    = $id;
        $model->date            = date('Y-m-d', strtotime($request->date));
        $model->time            = $request->time;
        $model->updated_by      = \Auth::id();
        $model->save();

        return back()->with(['alert' => 'success', 'message' => 'Hearing successfully updated.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function done($id)
    {
        $model                  = Hearing::find($id);
        $model->status          = 'done';
        $model->save();
        
        $model2                 = \App\Complaint::find($model->complaint_id);
        $model2->hearing_stage  = count(Hearing::where(['complaint_id' => $model->complaint_id, 'status' => 'done'])->get());
        $model2->save();

        return back()->with(['alert' => 'success', 'message' => 'Hearing successfully set to done.']);
    }
}
