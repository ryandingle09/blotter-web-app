<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hearings = \App\Hearing::orderBy('date', 'ASC')->limit(10)->get();
        $complaints = \App\Complaint::orderBy('id', 'DESC')->limit(10)->get();

        $h_count = \App\Hearing::count();
        $c_count = \App\Complaint::count();

        return view('dashboard.dashboard', [
            'hearings'      => $hearings,
            'complaints'    => $complaints,
            'h_count'       => $h_count,
            'c_count'       => $c_count
        ]);
    }
}
