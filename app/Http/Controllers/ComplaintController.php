<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ComplaintRequest;
use App\Complaint;


class ComplaintController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->get('search'))
        {
            $complaints = Complaint::where('complainant', 'like', '%'.$request->get('search').'%')
                ->orWhere('respondent', 'like', '%'.$request->get('search').'%')
                ->orWhere('complainant_age', 'like', '%'.$request->get('search').'%')
                ->orWhere('respondent_age', 'like', '%'.$request->get('search').'%')
                ->orWhere('complainant_address', 'like', '%'.$request->get('search').'%')
                ->orWhere('respondent_address', 'like', '%'.$request->get('search').'%')
                ->orWhere('hearing_stage', 'like', '%'.$request->get('search').'%')
                ->orWhere('status', 'like', '%'.$request->get('search').'%')
                ->orWhere('added_by', 'like', '%'.$request->get('search').'%')
                ->orderBy('id', 'DESC')
                ->with('user')->paginate(10);
            
            $complaints->withPath('?search='.$request->get('search'));
        }
        else
        {
            $complaints = Complaint::orderBy('id', 'DESC')->with('user')->paginate(10);
        }

        return view('complaint.index', ['complaints' => $complaints]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('complaint.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ComplaintRequest $request)
    {   
        $model = new Complaint;

        if ($request->hasFile('evidence')) 
        {
            $extensions = ['jpeg','bmp','png','jpg','gif','mp4','mpeg','avi','quicktime'];

            //check extensions
            foreach ($request->evidence as $photo)
            {
                $extension  = $photo->getClientOriginalExtension();
                $check      = in_array($extension, $extensions);

                if(!$check) return back()->withInput()->with('evidence', 'Upload file must be an jpeg|bmp|png|jpg|gif|mp4|mpeg|avi|quicktime file extensiont or valid image/video');

                $ext[]  = $check;
            }

            //set images
            foreach ($request->evidence as $photo)
            {
                $filename = $photo->store('files', 'public');
                $data[]     = url('/').'/storage/'.$filename;
            }
            
            $model->file  = json_encode($data);
        }

        $model->user_id             = \Auth::id();
        $model->complainant         = $request->complainant;
        $model->complainant_age     = $request->complainant_age;
        $model->complainant_address = $request->complainant_address;
        $model->respondent          = $request->respondent;
        $model->respondent_age      = $request->respondent_age;
        $model->respondent_address  = $request->respondent_address;
        $model->statement           = $request->statement;
        $model->added_by            = \Auth::id();
        $model->save();
        
        return back()->with(['alert' => 'success', 'message' => 'Complaint successfully added.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Complaint::find($id);
        return view('complaint.show', [
            'data' => $data, 
            'id' => $id
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Complaint::find($id);
        return view('complaint.edit', [
            'data' => $data, 
            'id' => $id
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Complaintrequest $request, $id)
    {
        $model                      = Complaint::find($id);
        
        if ($request->hasFile('evidence')) 
        {
            $extensions = ['jpeg','bmp','png','jpg','gif','mp4','mpeg','avi','quicktime'];

            //check extensions
            foreach ($request->evidence as $photo)
            {
                $extension  = $photo->getClientOriginalExtension();
                $check      = in_array($extension, $extensions);

                if(!$check) return back()->withInput()->with('evidence', 'Upload file must be an jpeg|bmp|png|jpg|gif|mp4|mpeg|avi|quicktime file extensiont or valid image/video');

                $ext[]  = $check;
            }

            //remove old images if has uploaded
            if(!is_null($model->file))
            {
                foreach(json_decode($model->file) as $photo)
                {
                    $db_image = str_replace(url('/').'/storage', storage_path('app/public'), $photo);
                    #return $db_image;
                    if(file_exists($db_image)) unlink($db_image);
                }
            }

            //update new images
            foreach ($request->evidence as $photo)
            {
                $filename = $photo->store('files', 'public');
                $data[]     = url('/').'/storage/'.$filename;
            }
            
            $model->file  = json_encode($data);
        }

        $model->complainant         = $request->complainant;
        $model->complainant_age     = $request->complainant_age;
        $model->complainant_address = $request->complainant_address;
        $model->respondent          = $request->respondent;
        $model->respondent_age      = $request->respondent_age;
        $model->respondent_address  = $request->respondent_address;
        $model->statement           = $request->statement;
        $model->updated_by          = \Auth::id();
        $model->save();

        return back()->with(['alert' => 'info', 'message' => 'Complaint successfully updated.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $model = Complaint::find($id);
        $model->delete();

        if($request->get('page') || $request->get('search'))
            return back()->with(['alert' => 'info', 'message' => 'Complaint successfully deleted.']);
        else
            return redirect('complaint')->with(['alert' => 'info', 'message' => 'Complaint successfully deleted.']);
    }

    public function done($id)
    {
        $model                  = Complaint::find($id);
        $model->status          = 'done';
        $model->hearing_stage   = 'completed';
        $model->save();
        
        $data = \App\Hearing::where('complaint_id', $id)->update([
            'status' => 'done'
        ]);

        return back()->with(['alert' => 'success', 'message' => 'Hearing successfully set to done.']);
    }
}
