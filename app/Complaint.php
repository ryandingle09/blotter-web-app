<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Complaint extends Model
{
    protected $fillable = [
        'user_id',
        'complainant',
        'complainant_age',
        'complainant_address',
        'statement',
        'respondent',
        'respondent_age',
        'respondent_address',
        'file',
        'added_by',
        'updated_by',
        'deleted_by',
        'deleted_at'
    ];

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function hearing()
    {
        return $this->hasMany('App\Hearing', 'id', 'complaint_id');
    }
}
