<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hearing extends Model
{
    protected $fillable = [
        'complaint_id',
        'date',
        'time',
        'status',
        'added_by',
        'updated_by'
    ];

    public function complaint()
    {
        return $this->hasOne('App\Complaint', 'id', 'complaint_id');
    }
}
