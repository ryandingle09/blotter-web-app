<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'email', 
        'username', 
        'firstname',
        'lastname', 
        'birthdate', 
        'address', 
        'added_by',
        'updated_by',
        'deleted_by',
        'deleted_at',
        'usertype',
        'password',
        'api_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 
        'remember_token',
    ];

    public function complaints()
    {
        return $this->hasMany('App\Complaint', 'complainant_id', 'id');
    }
}
