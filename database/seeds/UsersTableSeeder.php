<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Super Admin',
            'email' => 'admin@gmail.com',
            'username' => 'admin',
            'usertype' => '1',
            'firstname' => 'Super',
            'lastname' => 'Admin',
            'username' => 'admin',
            'password' => bcrypt('password'),
            'api_token' => bcrypt('admin'),
            'created_at' => date('Y-m-d m:i:s'),
        ]);
    }
}
