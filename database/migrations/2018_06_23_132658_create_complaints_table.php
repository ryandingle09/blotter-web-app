<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComplaintsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('complaints', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('complainant')->nullable();
            $table->integer('complainant_age')->nullable();
            $table->text('complainant_address')->nullable();
            $table->longText('statement')->nullable();
            $table->string('respondent')->nullable();
            $table->integer('respondent_age')->nullable();
            $table->text('respondent_address')->nullable();
            $table->text('file')->nullable();
            $table->string('hearing_stage', 10)->default('0');
            $table->string('status')->default('active');
            $table->integer('added_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('complaints');
    }
}
