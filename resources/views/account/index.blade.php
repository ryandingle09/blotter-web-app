@extends('layouts.app')

@section('title') - My Account @endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        @if(session('alert'))
            @alert(['type' => session('alert'), 'message' => session('message'), 'title' => session('alert')]) @endalert
        @endif

        @if(count($errors))
            @alert(['type' => 'danger', 'message' => 'Please fill up required fields below.', 'title' => 'Whoops' ]) @endalert
        @endif
        <div class="col-md-12 card" style="text-align: center">
            <br><br>
            <h1 class="page-header">My Account</h1>
            <br><br>
            <table class="table">
                <tbody>
                    <tr>
                        <td>First Name : <b>{{ $data->firstname }}</b></td>
                    </tr>
                    <tr>
                        <td>Last Name : <b>{{ $data->lastname }}</b></td>
                    </tr>
                    <tr>
                        <td>Email Address : <b>{{ $data->email }}</b></td>
                    </tr>
                    <tr>
                        <td>User Type : <b>{{ ($data->usertype == 1) ? 'Admin' : 'App User' }}</b></td>
                    </tr>
                    <tr>
                        <td>Created On : <b>{{ date('F j, Y', strtotime($data->created_at)) }}</b></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
