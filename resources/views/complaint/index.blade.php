@extends('layouts.app')

@section('title') - Complaint List @endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h2 class="page-header">
                <div class="row">
                    <div class="col-md-6">
                        Complaint List
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-8">
                                <form id="search-form" action="{{ route('complaint') }}" method="GET">
                                    <div class="input-group mb-3">
                                        <input value="{{ isset($_GET['search']) ? $_GET['search'] : '' }}" type="text" name="search" class="form-control float-right" placeholder="Search .." aria-label="search" aria-describedby="basic-addon2">
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-secondary" type="submit"><i class="fas fa-search"></i></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-4">
                            <a href="{{ route('create-complaint') }}" class="btn btn-default float-right btn-outline-primary"><i class="fas fa-plus"></i> Add Complaint</a>
                            </div>
                        </div>
                    </div>
                </div>
            </h2>
        </div>
        @if(session('alert'))
            @alert(['type' => session('alert'), 'message' => session('message'), 'title' => session('alert')]) @endalert
        @endif

        @if(count($errors))
            @alert(['type' => 'danger', 'message' => 'Please fill up required fields below.', 'title' => 'Whoops' ]) @endalert
        @endif
        <div class="col-md-12">
            <div class="card">
                <table class="table table-bordered" style="margin-bottom: 0">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Complainant</th>
                            <th scope="col">Respondent</th>
                            <th scope="col">Hearing Stage #</th>
                            <th scope="col">Status</th>
                            <th scope="col">Submitted On</th>
                            <th scope="col" style="width:15%; text-align: center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($complaints) == 0)
                        <tr>
                            <th scope="row" colspan="7">No complaints yet.</th>
                        </tr>
                        @endif
                        @foreach($complaints as $data)
                        <tr>
                            <th scope="row">{{ $data->id }}</th>
                            <td>{{ $data->complainant }}</td>
                            <td>{{ $data->respondent }}</td>
                            <td>{!! ($data->hearing_stage !== '0') ? '<span class="badge badge-info">'.$data->hearing_stage.'</span>' : 'Not yet started.' !!}</td>
                            <td>
                                {!! ($data->status == 'active') ? '<span class="badge badge-warning">'.ucfirst($data->status).'</span>' : '<span class="badge badge-success">'.ucfirst($data->status).' <i class="fas fa-check"></i></span>' !!}
                            </td>
                            <td>{{ date('F j, Y', strtotime($data->created_at)) }}</td>
                            <td style="width: 15%; text-align: center">
                                @if($data->status == 'active')
                                <a href="{{ route('complaint') }}/{{ $data->id }}/done" data-toggle="tooltip" data-placement="top" title="Tag as Done"><span class="badge badge-success"><i class="fas fa-check"></i></span></a>
                                <a href="{{ route('hearing') }}/{{ $data->id }}/create" data-toggle="tooltip" data-placement="top" title="Add Hearing"><span class="badge badge-warning"><i class="fas fa-plus"></i></span></a>
                                <a href="{{ route('complaint') }}/{{ $data->id }}/edit"data-toggle="tooltip" data-placement="top" title="Edit complaint details"><span class="badge badge-info"><i class="fas fa-edit"></i></span></a>
                                <a href="{{ route('complaint') }}/{{ $data->id }}/show" data-toggle="tooltip" data-placement="top" title="View complaint details"><span class="badge badge-primary"><i class="fas fa-eye"></i></span></a>
                                <a href="javacript:;" onclick="event.preventDefault();
                                                    document.getElementById('delete-form').submit();" data-toggle="tooltip" data-placement="top" title="Delete Complaint">
                                    <span class="badge badge-danger"><i class="fas fa-trash"></i></span>
                                </a>

                                <form id="delete-form" action="{{ route('complaint') }}/{{ $data->id }}/delete{{ isset($_GET['page']) ? '?page='.$_GET['page'].'' : '' }}" method="POST" style="display: none;">
                                    @method('DELETE')->
                                    @csrf
                                </form>
                                @else
                                <a href="{{ route('complaint') }}/{{ $data->id }}/show" data-toggle="tooltip" data-placement="top" title="View complaint details"><span class="badge badge-primary"><i class="fas fa-eye"></i></span></a>
                                <a href="javacript:;" onclick="event.preventDefault();
                                                    document.getElementById('delete-form').submit();" data-toggle="tooltip" data-placement="top" title="Delete Complaint">
                                    <span class="badge badge-danger"><i class="fas fa-trash"></i></span>
                                </a>

                                <form id="delete-form" action="{{ route('complaint') }}/{{ $data->id }}/delete{{ isset($_GET['page']) ? '?page='.$_GET['page'].'' : '' }}" method="POST" style="display: none;">
                                    @method('DELETE')->
                                    @csrf
                                </form>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class=" col-md-12">
            <br>
            <div class="pagination">{{ $complaints->links() }} </div>
        </div>
    </div>
</div>
@endsection
