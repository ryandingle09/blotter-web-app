@extends('layouts.app')

@section('title') - Complaint Details @endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h2 class="page-header">
                Complaint Details
                <a href="{{ route('complaint') }}" class="btn btn-default float-right btn-outline-primary"><i class="fas fa-arrow-left"></i> Back</a>
            </h2>
        </div>
        <div class="col-md-12">
            <div class="card">
                <table class="table table-bordered" style="margin-bottom: 0">
                    <thead>
                        <tr>
                            <th scope="col" style="text-align: right">
                                <a href="{{ route('complaint') }}/{{ $id }}/edit" class="btn btn-info"><i class="fas fa-edit"></i> Edit</a>
                                <button type="button" class="btn btn-danger" 
                                    onclick="event.preventDefault();
                                                    document.getElementById('delete-form').submit();">
                                    <i class="fas fa-trash"></i> Delete
                                </button>

                                <form id="delete-form" action="{{ route('complaint') }}/{{ $id }}/delete" method="POST" style="display: none;">
                                    @method('DELETE')
                                    @csrf
                                </form>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">Complaint #<b>{{ $data->id }}</b></th>
                        </tr>
                        <tr>
                            <td>Complainant : <b>{{ $data->complainant }}</b></td>
                        </tr>
                        <tr>
                            <td>Complainant Age : <b>{{ $data->complainant_age }}</b></td>
                        </tr>
                        <tr>
                            <td>Complainant Address : <b>{{ $data->complainant_address }}</b></td>
                        </tr>
                        <tr>
                            <td>Respondent : <b>{{ $data->respondent }}</b></td>
                        </tr>
                        <tr>
                            <td>Respondent Age : <b>{{ $data->respondent_age }}</b></td>
                        </tr>
                        <tr>
                            <td>Respondent Address : <b>{{ $data->respondent_address }}</b></td>
                        </tr>
                        <tr>
                            <td>
                                Statement : <br><br>
                                {!! $data->statement !!}
                                <br>
                                <br>
                                @if(!is_null($data->file))
                                    <b>Evidences</b>
                                    <br>
                                    <br>
                                    <div class="row">
                                        @foreach(json_decode($data->file) as $file)
                                        <a href="{{$file}}" target="_blank">
                                        <div class="card" style="width: 18rem;margin: 15px">
                                            <img class="card-img-top" src="{{$file}}" alt="{{$file}}">
                                        </div>
                                        </a>
                                        @endforeach
                                    </div>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>Submitted By : <b>{{ $data->user['name'] }}</b></td>
                        </tr>
                        <tr>
                            <td>Submitted On : <b>{{ date('F j, Y', strtotime($data->created_at)) }}</b></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
