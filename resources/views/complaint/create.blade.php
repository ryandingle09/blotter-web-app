@extends('layouts.app')

@section('title') - Add Complaint @endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h2 class="page-header">
                Add Complaint
                <a href="{{ route('complaint') }}" class="btn btn-default float-right btn-outline-primary"><i class="fas fa-arrow-left"></i> Back</a>
            </h2>
        </div>
        @if(session('alert'))
            @alert(['type' => session('alert'), 'message' => session('message'), 'title' => session('alert')]) @endalert
        @endif

        @if(count($errors))
            @alert(['type' => 'danger', 'message' => 'Please fill up required fields below.', 'title' => 'Whoops' ]) @endalert
        @endif
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form method="post" action="{{ route('store-complaint') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="complainant_name">Complainant Name</label>
                                    <input name="complainant" value="{{ old('complainant') }}" type="text" class="form-control @if ($errors->has('complainant')) is-invalid @endif" id="complainant" aria-describedby="complainant_nameHelp" placeholder="Complainant Name" autofocus>
                                    <small id="complainant_nameHelp" class="form-text text-muted">Enter complainant name.</small>
                                    @if ($errors->has('complainant_name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('complainant') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="complainant_age">Age</label>
                                    <input name="complainant_age" value="{{ old('complainant_age') }}" type="number" class="col-md-2 form-control @if ($errors->has('complainant_age')) is-invalid @endif" id="complainant_age" aria-describedby="complainant_ageHelp" placeholder="Age">
                                    <small id="complainant_ageHelp" class="form-text text-muted">Enter complainant age.</small>
                                    @if ($errors->has('complainant_age'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('complainant_age') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="complainant_address">Address</label>
                                    <textarea name="complainant_address" class="form-control @if ($errors->has('complainant_address')) is-invalid @endif" id="complainant_address" aria-describedby="complainant_addressHelp" placeholder="Complainant Address">{{ old('complainant_address') }}</textarea>
                                    <small id="complainant_addressHelp" class="form-text text-muted">Enter complainant address.</small>
                                    @if ($errors->has('complainant_address'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('complainant_address') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="respondent_name">Respondent Name</label>
                                    <input name="respondent" value="{{ old('respondent') }}" type="text" class="form-control @if ($errors->has('respondent')) is-invalid @endif" id="respondent" aria-describedby="respondent_nameHelp" placeholder="Respondent Name">
                                    <small id="respondent_nameHelp" class="form-text text-muted">Enter respondent name.</small>
                                    @if ($errors->has('respondent_name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('respondent') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="respondent_age">Age</label>
                                    <input name="respondent_age" value="{{ old('respondent_age') }}" type="number" class="col-md-2 form-control @if ($errors->has('respondent_age')) is-invalid @endif" id="respondent_age" aria-describedby="respondent_ageHelp" placeholder="Age">
                                    <small id="respondent_ageHelp" class="form-text text-muted">Enter respondent age.</small>
                                    @if ($errors->has('respondent_age'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('respondent_age') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="respondent_address">Address</label>
                                    <textarea name="respondent_address" class="form-control @if ($errors->has('respondent_address')) is-invalid @endif" id="respondent_address" aria-describedby="respondent_addressHelp" placeholder="Respondent Address">{{ old('respondent_address') }}</textarea>
                                    <small id="respondent_addressHelp" class="form-text text-muted">Enter respondent address.</small>
                                    @if ($errors->has('respondent_address'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('respondent_address') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="statemente">Statement</label>
                            <textarea name="statement" class="form-control statement @if ($errors->has('statement')) is-invalid @endif" id="statement" aria-describedby="statementHelp" placeholder="Statement">{{ old('statement') }}</textarea>
                            <small id="statementHelp" class="form-text text-muted">Write your statement details.</small>
                            @if ($errors->has('statement'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('statement') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Supported Files/Evidence (Optional)</label>
                            <input type="file" name="evidence[]" class="form-control @if (session('evidence')) is-invalid @endif" multiple="true">
                            <small id="evidenceHelp" class="form-text text-muted">Upload supported documents.</small>
                            @if (session('evidence'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ session('evidence') }}</strong>
                                </span>
                            @endif
                        </div>
                        <button type="submit" class="btn btn-primary float-right">Submit <i class="fas fa-save"></i></button>
                    </form>
                </div>  
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript" src="{{ asset('js/tinymice.js') }}"></script>
<script type="text/javascript">tinymce.init({ selector:'.statement' });</script>
@endpush