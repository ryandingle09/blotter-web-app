@extends('layouts.app')

@section('title') - Dashboard @endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h4 class="page-header">Hearings <span class="badge badge-primary">{{ $h_count }}  - total records found.</span></h4>
            <div class="card">
                <table class="table table-bordered" style="margin-bottom: 0">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Complaint Id #</th>
                            <th scope="col">Date</th>
                            <th scope="col">Time</th>
                            <th scope="col">Status</th>
                            <th scope="col">Submitted On</th>
                            <th scope="col" style="width: 18%; text-align: center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($hearings) == 0)
                        <tr>
                            <th scope="row" colspan="6">No hearings yet.</th>
                        </tr>
                        @endif
                        @foreach($hearings as $data)
                        <tr>
                            <th scope="row">{{ $data->id }}</th>
                            <td><a href="{{ route('complaint') }}/{{ $data->complaint_id }}/show">{{ $data->complaint_id }}</a></td>
                            <td>{{ date('F j, Y', strtotime($data->date)) }}</td>
                            <td>{{ date('h:i A', strtotime($data->time)) }}</td>
                            <td>
                                {!! ($data->status == 'active') ? '<span class="badge badge-warning">'.ucfirst($data->status).'</span>' : '<span class="badge badge-success">'.ucfirst($data->status).'</span>' !!}
                            </td>
                            <td>{{ date('F j, Y', strtotime($data->created_at)) }}</td>
                            <td style="width: 18%; text-align: center">
                                <a href="{{ route('hearing') }}/{{ $data->id }}/show"><span class="badge badge-primary"><i class="fas fa-eye"></i></span></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        <br>
        <br>
        </div>
        <hr>
        <div class="col-md-12">
            <h4 class="page-header">Complaints <span class="badge badge-primary">({{ $c_count }} - total records found.)</span></h4>
            <div class="card">
                <table class="table table-bordered" style="margin-bottom: 0">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Complainant</th>
                            <th scope="col">Respondent</th>
                            <th scope="col">Hearing Stage #</th>
                            <th scope="col">Status</th>
                            <th scope="col">Submitted On</th>
                            <th scope="col" style="width: 18%; text-align: center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($complaints) == 0)
                        <tr>
                            <th scope="row" colspan="7">No complaints yet.</th>
                        </tr>
                        @endif
                        @foreach($complaints as $data)
                        <tr>
                            <th scope="row">{{ $data->id }}</th>
                            <td>{{ $data->complainant }}</td>
                            <td>{{ $data->respondent }}</td>
                            <td>{!! ($data->hearing_stage !== '0') ? '<span class="badge badge-info">'.$data->hearing_stage.'</span>' : 'Not yet started.' !!}</td>
                            <td>
                                {!! ($data->status == 'active') ? '<span class="badge badge-warning">'.ucfirst($data->status).'</span>' : '<span class="badge badge-success">'.ucfirst($data->status).'</span>' !!}
                            </td>
                            <td>{{ date('F j, Y', strtotime($data->created_at)) }}</td>
                            <td style="width: 18%; text-align: center">
                                <a href="{{ route('complaint') }}/{{ $data->id }}/show" data-toggle="tooltip" data-placement="top" title="View complaint details"><span class="badge badge-primary"><i class="fas fa-eye"></i></span></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
