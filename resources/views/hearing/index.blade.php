@extends('layouts.app')

@section('title') - Hearing List @endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h2 class="page-header">
                <div class="row">
                    <div class="col-md-9">
                        Hearing List
                    </div>
                    <div class="col-md-3">
                        <div class="row">
                            <div class="col-md-12">
                                <form id="search-form" action="{{ route('hearing') }}" method="GET">
                                    <div class="input-group mb-3">
                                        <input value="{{ isset($_GET['search']) ? $_GET['search'] : '' }}" type="text" name="search" class="form-control float-right" placeholder="Search .." aria-label="search" aria-describedby="basic-addon2">
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-secondary" type="submit"><i class="fas fa-search"></i></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </h2>
        </div>
        @if(session('alert'))
            @alert(['type' => session('alert'), 'message' => session('message'), 'title' => session('alert')]) @endalert
        @endif

        @if(count($errors))
            @alert(['type' => 'danger', 'message' => 'Please fill up required fields below.', 'title' => 'Whoops' ]) @endalert
        @endif
        <div class="col-md-12">
            <div class="card">
                <table class="table table-bordered" style="margin-bottom: 0">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Complaint Id #</th>
                            <th scope="col">Date</th>
                            <th scope="col">Time</th>
                            <th scope="col">Status</th>
                            <th scope="col">Submitted On</th>
                            <th scope="col" style="width: 10%; text-align: center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($hearings) == 0)
                        <tr>
                            <th scope="row" colspan="6">No hearings yet.</th>
                        </tr>
                        @endif
                        @foreach($hearings as $data)
                        <tr>
                            <th scope="row">{{ $data->id }}</th>
                            <td><a href="{{ route('complaint') }}/{{ $data->complaint_id }}/show">{{ $data->complaint_id }}</a></td>
                            <td>{{ date('F j, Y', strtotime($data->date)) }}</td>
                            <td>{{ date('h:i A', strtotime($data->time)) }}</td>
                            <td>
                                {!! ($data->status == 'active') ? '<span class="badge badge-warning">'.ucfirst($data->status).'</span>' : '<span class="badge badge-success">'.ucfirst($data->status).' <i class="fas fa-check"></i> </span>' !!}
                            </td>
                            <td>{{ date('F j, Y', strtotime($data->created_at)) }}</td>
                            <td style="width: 10%; text-align: center">
                                <a href="{{ route('hearing') }}/{{ $data->id }}/show" data-toggle="tooltip" data-placement="top" title="View Hearing Details"><span class="badge badge-primary"><i class="fas fa-eye"></i></span></a>
                                @if($data->status == 'active')
                                <a href="{{ route('hearing') }}/{{ $data->id }}/edit"><span class="badge badge-info" data-toggle="tooltip" data-placement="top" title="Edit Hearing Details"><i class="fas fa-edit"></i></span></a>                          
                                <a href="{{ route('hearing') }}/{{ $data->id }}/done"><span class="badge badge-success" data-toggle="tooltip" data-placement="top" title="Tag as Done"><i class="fas fa-check"></i></span></a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class=" col-md-12">
            <br>
            <div class="pagination">{{ $hearings->links() }}</div>
        </div>   
    </div>
</div>
@endsection
