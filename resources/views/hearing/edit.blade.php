@extends('layouts.app')

@section('title') - Edit Hearing @endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h2 class="page-header">
                Edit Hearing
                <a href="{{ route('hearing') }}" class="btn btn-default float-right btn-outline-primary"><i class="fas fa-arrow-left"></i> Back</a>
            </h2>
        </div>
        @if(session('alert'))
            @alert(['type' => session('alert'), 'message' => session('message'), 'title' => session('alert')]) @endalert
        @endif

        @if(count($errors))
            @alert(['type' => 'danger', 'message' => 'Please fill up required fields below.', 'title' => 'Whoops' ]) @endalert
        @endif
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form method="post" action="{{ route('hearing') }}/{{ $id }}/edit">
                        @method('PUT')
                        @csrf
                        <div class="form-group">
                            <label for="date">Hearing Date Schedule</label>
                            <input name="date" type="date" class="form-control @if ($errors->has('date')) is-invalid @endif" id="date" value="{{ ($data->date) ? date('Y-m-d', strtotime($data->date)) : '' }}" aria-describedby="dateHelp" placeholder="Date">
                            <small id="dateHelp" class="form-text text-muted">Please specify date.</small>
                            @if ($errors->has('date'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('date') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="time">Hearing Time Schedule</label>
                            <input name="time" type="time" class="form-control @if ($errors->has('time')) is-invalid @endif" id="date" value="{{ ($data->time) ? $data->time : '' }}" aria-describedby="dateHelp" placeholder="Time">
                            <small id="dateHelp" class="form-text text-muted">Please specify time.</small>
                            @if ($errors->has('time'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('time') }}</strong>
                                </span>
                            @endif
                        </div>
                        <button type="submit" class="btn btn-primary float-right">Update Hearing <i class="fas fa-save"></i></button>
                    </form>
                </div>  
            </div>
            <br>
            <h4>Complaint Details</h4>
            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <th scope="row">Complaint #<b>{{ $data2->id }}</b></th>
                    </tr>
                    <tr>
                        <td>Complainant : <b>{{ $data2->complainant }}</b></td>
                    </tr>
                    <tr>
                        <td>Complainant Age : <b>{{ $data2->complainant_age }}</b></td>
                    </tr>
                    <tr>
                        <td>Complainant Address : <b>{{ $data2->complainant_address }}</b></td>
                    </tr>
                    <tr>
                        <td>Respondent : <b>{{ $data2->respondent }}</b></td>
                    </tr>
                    <tr>
                        <td>Respondent Age : <b>{{ $data2->respondent_age }}</b></td>
                    </tr>
                    <tr>
                        <td>Respondent Address : <b>{{ $data2->respondent_address }}</b></td>
                    </tr>
                    <tr>
                        <td>
                            Statement : <br><br>
                            {!! $data2->statement !!}
                        </td>
                    </tr>
                    <tr>
                        <td>Submitted By : <b>{{ $data2->user['name'] }}</b></td>
                    </tr>
                    <tr>
                        <td>Submitted On : <b>{{ date('F j, Y', strtotime($data2->created_at)) }}</b></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection