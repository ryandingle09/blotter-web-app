@extends('layouts.app')

@section('title') - Hearing Details @endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h2 class="page-header">
                Hearing Details
            </h2>
        </div>
        @if(session('alert'))
            @alert(['type' => session('alert'), 'message' => session('message'), 'title' => session('alert')]) @endalert
        @endif

        @if(count($errors))
            @alert(['type' => 'danger', 'message' => 'Please fill up required fields below.', 'title' => 'Whoops' ]) @endalert
        @endif
        <div class="col-md-12">
            <div class="card">
                <table class="table table-bordered" style="margin-bottom: 0">
                    <tbody>
                        <tr>
                            <td>Hearing Date # <b>{{ date('F j, Y', strtotime($data->date)) }}</b></td>
                        </tr>
                        <tr>
                            <td>Hearing Time : <b>{{ date('h:i A', strtotime($data->time)) }}</b></td>
                        </tr>
                        <tr>
                            <td>Submitted By : <b>{{ $data->user['name'] }}</b></td>
                        </tr>
                        <tr>
                            <td>Submitted On : <b>{{ date('F j, Y', strtotime($data->created_at)) }}</b></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection