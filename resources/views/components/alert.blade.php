<div class="col-md-12">
    @if($type == 'success')
    <div class="alert alert-success alert-dismissible fade show" role="alert">
    @endif
    @if($type == 'info')
    <div class="alert alert-info alert-dismissible fade show" role="alert">
    @endif
    @if($type == 'warning')
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
    @endif
    @if($type == 'danger')
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
    @endif
        @if($title) <strong>{{ ($title == 'info' || $title == 'success') ? 'Success' : ucfirst($title) }} !</strong> @endif
        {{ $message }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
</div>