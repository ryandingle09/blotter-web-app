@extends('layouts.app')

@section('title') - User List @endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        @if(session('alert'))
            @alert(['type' => session('alert'), 'message' => session('message'), 'title' => session('alert')]) @endalert
        @endif

        @if(count($errors))
            @alert(['type' => 'danger', 'message' => 'Please fill up required fields below.', 'title' => 'Whoops' ]) @endalert
        @endif
        <div class="col-md-12">

        </div>
    </div>
</div>
@endsection
