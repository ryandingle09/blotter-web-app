let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
    mix.js('./node_modules/@fortawesome/fontawesome-free/js/all.js', 'public/js/fontawesome.js')
    mix.js('resources/assets/js/tinymice.js', 'public/js/tinymice.js')
   .sass('resources/assets/sass/app.scss', 'public/css');
